import { Component } from '@angular/core';
import { AuthUserService } from './services/auth-user.service';
import { NotificationService } from './services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Test-client';
  public notifications = [];

  public Auth=false;
  constructor(
   private router: Router,
   private notificationService: NotificationService
  ){
    
    this.notificationService.getNotification().subscribe(
      res => {
          this.notifications.splice(0, 0, res)
      })
   this.checkAuth()
  }
  checkAuth(){
    if(localStorage.getItem('access_token')) {
        AuthUserService.setLoggedInStatus(true);
        this.Auth = true
    }else{
        AuthUserService.setLoggedInStatus(false);
        this.Auth = false
        this.router.navigate(['/login']);
    }
  }
  closeNotification(index){
    this.notifications.splice(index, 1)
  }
}
