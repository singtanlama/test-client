import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthUserService } from './services/auth-user.service';
import { LoginService } from './services/login.service';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
     path: '',
      redirectTo: 'employee',
      pathMatch: 'full'
    },
     {
      path: 'employee',
      loadChildren: () => import('./pages/employee-page/employee-page.module').then(m => m.EmployeePageModule),
      // canActivate:[AuthGuardService],
      },
      {
      path: 'login',
      loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
      }
 ];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    LoginService,
    AuthUserService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
