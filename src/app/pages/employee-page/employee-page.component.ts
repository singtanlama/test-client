import { Component, OnInit } from '@angular/core';
import { ImportService } from 'src/app/services/import.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ENV } from 'src/app/env';
import { ExportService } from 'src/app/services/export.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Router } from "@angular/router";
import { AuthUserService } from 'src/app/services/auth-user.service';
declare var jQuery: any;
declare var $;
import * as jspdf from 'jspdf'; 
import html2canvas from 'html2canvas'; 
 
@Component({
  selector: 'app-employee-page',
  templateUrl: './employee-page.component.html',
  styleUrls: ['./employee-page.component.scss']
})
export class EmployeePageComponent implements OnInit {

  constructor(private _import_service:ImportService,
    private _fb:FormBuilder ,
    private _employee_service:EmployeeService,
    public _sanitizer: DomSanitizer,
    private _export_service:ExportService,
    private notification: NotificationService,
    private router:Router
    ) { }


public importFile =this._fb.group({
  file_import:new FormControl()
}); 
statuscollapse =false;

public file: File;

submit=false

public fileToUpload: File; 
 
uploadFlag=false;

logoutFlag=false

private profilePic: string = '/assets/images/default-image.jpg';

public secureProfilePic: any = null;
  
employeeId;

Submit="Submit";

Choose="Choose Image"

public employees=[];

ChooseExcel="Choose Excel File"

public status=false

employeeCheckLists=[]

checkForm:FormGroup

createdDateLists=[]
public paginationIndex = null;

is_edit=false;
ListForExport=[]



employeeNotExisiFlag=false

image; 

CreateEmployeeForm=this._fb.group({
      profileImage:new FormControl,
      full_name :new FormControl("",Validators.required),
      date:new FormControl("",Validators.required),
      gender:new FormControl("",Validators.required),
      designation:new FormControl("",Validators.required),
      salary:new FormControl("",Validators.required)
});



imageServer:string

ngOnInit() {

this.imageServer=ENV.imageServer


this.initialGetEmployee();
}
public initialGetEmployee(){
  
this._employee_service.getEmployeeRedord(this.paginationIndex).subscribe(
  res=>{
  console.log(res)
  this.employees= res.employee.data; 
  if(this.employees.length <= 0){
    this.employeeNotExisiFlag=true
  } else{
    this.employeeNotExisiFlag=false

  }
  this.createdDateLists=res.date

  this.paginationInfo.currentPage = res.employee.current_page
  this.paginationInfo.from = res.employee.from
  this.paginationInfo.lastPage = res.employee.last_page
  this.paginationInfo.perPage = res.employee.per_page
  this.paginationInfo.to = res.employee.to
  this.paginationInfo.total = res.employee.total
  this.paginationInfo.pageComponent = []
  let counter = 0
  for (let i = 1; i <= res.employee.last_page; i++) {

  if (res.employee.last_page > 5 && i + 1 == res.employee.last_page) {
    this.paginationInfo.pageComponent.push({
      'index': '...',
      'click': false,
      'active': false
    })
  } else {
    this.paginationInfo.pageComponent.push({
      'index': i,
      'click': true,
      'active': i == res.employee.current_page ? true : false
    })
  }
    counter++;
    if (counter == 4 && res.last_page >= 5) i = res.last_page - 2
    }
     this.initialCheckList() 
  })
}

public initialCheckList(){
  for(let emp in this.employees){
    this.employeeCheckLists.push(this._fb.group({
      selected: this.status,
      id:this.employees[emp].id,
   }));
}


this.checkForm=this._fb.group({
checkList:this._fb.array(this.employeeCheckLists),
})

console.log(this.checkForm)
}



public selectAllCheckbox(event){
    this.status=event.currentTarget.checked;
    this.employeeCheckLists=[]
    this.initialCheckList() 
}


public selectCreatedDate(date){
console.log(date.target.value)
this._employee_service.getEmployeebyCreatedDate(date.target.value).subscribe(
res=>{
   console.log(res)
   this.employees= res.employee.data;  
    }
  )
}


public paginationInfo = {
    currentPage: null,
    from: null,
    lastPage: null,
    perPage: null,
    pageComponent: [],
    to: null,
    total: null
}



public uploadFile(file){
    console.log('afdshgfajhgd',file)
    this._import_service.importCSVandExcelFile(file).subscribe(
        res=>{
     console.log(res)
      })
}




public ChangeProfilePicture(event: EventTarget) {
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    if (files[0].size / 4096 / 1024 > 1) {
    console.log('fajdfbadj')
    alert('File must be less than 4 MB');
    } else {
    this.file = files[0];
    this.profilePic = this.file.name;
    console.log(this.profilePic)
    this.Choose=this.profilePic

    this.secureProfilePic = this._sanitizer.bypassSecurityTrustStyle("url('" + URL.createObjectURL(files[0]) + "')");
     console.log(this.secureProfilePic)
    }
}



public create(data){
  if(this.employeeId && this.is_edit){
    data['id']=this.employeeId;
    this.submit=true
    this.Submit="Updating....";

  }else{
    this.submit=true
    this.Submit="Submitting....";
  }
  console.log(this.is_edit)
  if(this.CreateEmployeeForm.valid){
    this._employee_service.registerEmployee(this.is_edit,data,this.file).subscribe(
      res=>{
       console.log(res)
       this.Choose="Choose Image"
       if(this.is_edit){
          this.is_edit=false;
          this.notification.pushNotification('Successfully Updated Employee', 'success');
       }else{
        this.notification.pushNotification('Successfully Inserted Employee', 'success');
       }
       this.employeeId;
       this.Submit="Submit";
       this.submit=false
       jQuery("#collapseID").click();
       this.CreateEmployeeForm.reset()
       this.initialGetEmployee();
    })
  }else{
    this.notification.pushNotification('Internal server Error', 'danger');
    this.submit=false
    if(this.is_edit){
      this.Submit="Update";
    }else{
      this.Submit="Submit";
    }
    console.log('invalid')
  }
}



importCSVandExcelFile(event: EventTarget){
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    console.log(files)
    this.fileToUpload=files[0]
    this.ChooseExcel = files[0].name
}
UploadFile(){
  if(this.fileToUpload){
    this.uploadFlag=true;
    this._import_service.importCSVandExcelFile(this.fileToUpload).subscribe(
      res=>{
        this.uploadFlag=false;
       console.log(res)
      if(res.flag == 0){

        this.notification.pushNotification(res.res +' '+ res.index , 'danger');

    }else{
         this.ChooseExcel="Choose Excel File"

         this.notification.pushNotification(res.res + ' ' +'but Null Index are ' + '['+ res.index + ']', 'success');
        
         this.initialGetEmployee();
        }
   })
  }else{
    this.notification.pushNotification('please select file to upload', 'danger');

  }

}

collapseController(){
 if (this.statuscollapse) {
     this.statuscollapse = false;
   } else {
     this.statuscollapse = true;
  }
}


editEmployee(i){
      this.is_edit=true;
      this.employeeId=this.employees[i].id;
      this.CreateEmployeeForm.controls.full_name.setValue(this.employees[i].full_name)
      this.CreateEmployeeForm.controls.date.setValue(this.employees[i].date_of_birth)
      this.CreateEmployeeForm.controls.designation.setValue(this.employees[i].designation)
      this.CreateEmployeeForm.controls.gender.setValue(this.employees[i].gender)
      this.CreateEmployeeForm.controls.salary.setValue(this.employees[i].salary)
      this.image=this.employees[i].image;
      console.log('image',this.image)
      this.Submit="Update";
      if (!this.statuscollapse) {
          jQuery("#collapseID").click();
      }
}



loadClassList(index, prev, next) {
   let reqIndex = null
     if (prev && this.paginationInfo.currentPage != 1) {
          reqIndex = this.paginationInfo.currentPage - 1
      } else if (next && this.paginationInfo.lastPage != this.paginationInfo.currentPage) {
          reqIndex = this.paginationInfo.currentPage + 1
      } else if (index != null) {
           reqIndex = index
      } else {
          return
      }




console.log(reqIndex, prev, next)
      this.paginationIndex = reqIndex;
      this._employee_service.getEmployeeRedord(reqIndex).subscribe(res => {
      console.log(res)
      this.employees = res.employee.data;
      this.paginationInfo.currentPage = res.employee.current_page
      this.paginationInfo.from = res.employee.from
      this.paginationInfo.lastPage = res.employee.last_page
      this.paginationInfo.perPage = res.employee.per_page
      this.paginationInfo.to = res.employee.to
      this.paginationInfo.total = res.employee.total
      this.paginationInfo.pageComponent = []
      let counter = 0
      for (let i = 1; i <= res.employee.last_page; i++) {

      if (res.employee.last_page > 5 && i + 1 == res.employee.last_page) {
      this.paginationInfo.pageComponent.push({
        'index': '...',
        'click': false,
        'active': false
      })
      } else {
      this.paginationInfo.pageComponent.push({
         'index': i,
         'click': true,
         'active': i == res.employee.current_page ? true : false
        })
      }
        counter++;
         if (counter == 4 && res.last_page >= 5) i = res.last_page - 2
      }
   }, err => {

  })
}




ExcelExport(){
    this.ListForExport=[] 
    for(let emp in this.employees){
    let flag=false
    for(let check in this.checkForm.controls.checkList.value){
      if(this.checkForm.controls.checkList.value[check].selected && this.checkForm.controls.checkList.value[check].id == this.employees[emp].id){
          flag=true
      }   
    }
    if(flag){
      this.ListForExport.push({
          'Full Name':this.employees[emp].full_name,
          'Date of Birth':this.employees[emp].date_of_birth,
          'Designation':this.employees[emp].designation,
          'Gender':this.employees[emp].gender,
          'Salary':this.employees[emp].salary,
       })
      }
    }
    return true;     
 }


  async downloadPdfAndExcel(){
     const flag = await this.ExcelExport();
    if(flag){
       if(this.ListForExport.length > 0 ){
             this._export_service.exportAsExcelFile(this.ListForExport, 'sample')
           }else{
             this.notification.pushNotification('Select the Employee List', 'danger');
         }
      }
  }    
 
 


  Reset(){
    this.is_edit=false;
    this.Submit="Submit";
    this.CreateEmployeeForm.reset()
  }  
  
  
  LogOut(){
    this.logoutFlag=true
    this._employee_service.Logout().subscribe(
      res=>{
        console.log(res)
        this.logoutFlag=false

        localStorage.removeItem('access_token');
        AuthUserService.setLoggedInStatus(false);
        this.router.navigate(["/login"]);
      }
    )
  }
}
