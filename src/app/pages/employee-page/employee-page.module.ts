import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeePageComponent } from './employee-page.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ImportService } from 'src/app/services/import.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
      path: '',
      component: EmployeePageComponent,
      data: {
          title: 'Employee Page | Login'
      }
  }
];

@NgModule({
  declarations: [EmployeePageComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  providers:[
    ImportService,
    EmployeeService
  ]
})
export class EmployeePageModule { }
