import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'src/app/services/auth-user.service';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public submitButtonStatus = true;

  public email = new FormControl('', Validators.required)
  public password = new FormControl('', Validators.required)

  public loginForm = this.formBuilder.group({
      "email": this.email,
      "password": this.password
  })

  private url
  constructor(
    private formBuilder: FormBuilder,
                private notification: NotificationService,
                private router:Router,
                private loginService: LoginService
  ) { 
    this.url = this.router.url
  }

  ngOnInit() {
  }
  public submitForm(data: FormData) {
    console.log(data)
    if (this.loginForm.valid) {
        this.submitButtonStatus = false;
        this.loginService.login(data).subscribe(
          res => {
              console.log(res)
              this.notification.pushNotification('You are logged in', 'success');
              localStorage.setItem('access_token',res.access_token);
              AuthUserService.setLoggedInStatus(true)
              this.router.navigate([this.url=="/login" || this.url==""?'/employee':this.url]);
          }, err => {
              this.submitButtonStatus = true;
              if (err.status === 401) {
                  this.notification.pushNotification('You are not authorized user', 'danger');
              } else {
                  this.notification.pushNotification('Sorry the credential missmatch', 'danger');
              }
           }
        )
     } 
  }

}
