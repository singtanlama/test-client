import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
const routes: Routes = [
  {
      path: '',
      component: LoginComponent,
      data: {
          title: 'Test | Login'
      }
  }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forChild(routes)
  ],
  providers:[
    LoginService
  ]
})
export class LoginModule { }
