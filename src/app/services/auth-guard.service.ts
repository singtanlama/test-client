import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthUserService } from './auth-user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  public Auth;
  constructor(public router: Router){
    console.log('fadjsfgjad')
    this.Auth = AuthUserService.getLoggedInStatus()
    AuthUserService.loggedInChanged.subscribe((res)=>{
      this.Auth=res
      console.log(res,this.Auth)
    },err=>{

    })
  }
  canActivate() {
    if (this.Auth) {
      this.router.navigate(['/employee']);
      return true;
    }
    return false;
  }
}
