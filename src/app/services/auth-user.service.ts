import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthUserService {

  constructor() { }

  public static loggedIn:boolean = false;
  public static loggedInChanged:EventEmitter<any> = new EventEmitter();
  public static setLoggedInStatus(status){
    console.log(status)
    AuthUserService.loggedIn = status
    AuthUserService.loggedInChanged.emit(status);
  }

  public static getLoggedInStatus(){
    return AuthUserService.loggedIn
  }
}
