import { Injectable } from '@angular/core';

import {Http, Response}          from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { ENV } from '../env';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private _http:Http,public http:HttpClient) { }
  getEmployeeRedord(index): Observable<any> {
  let headers = new Headers({
    'mimeType': "multipart/form-data",
    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
  });
  let options = new RequestOptions({headers: headers, withCredentials: true});
  return this._http.get(ENV.apiServer + '/get-employee-record'+ '?page=' + index, options)
      .map(this.extractData)
      .catch(this.handleError);
}

public extractData(res: Response) {
  let response = res.json();
  return response || {};
}

public handleError(error: Response | any) {
  return Observable.throw(error);
}



getEmployeebyCreatedDate(id){
  let headers = new Headers({
    'mimeType': "multipart/form-data",
    'Authorization': 'Bearer ' + localStorage.getItem('access_token')
  });
  let options = new RequestOptions({headers: headers, withCredentials: true});
  return this._http.get(ENV.apiServer + '/get-employee-by-created-date/'+id, options)
      .map(this.extractData)
      .catch(this.handleError);
  }




  registerEmployee(is_edit,data,file){
    let formData: FormData = new FormData();
    if (file) {
        formData.append('image', file, file.name);
    }
    formData.append('data', JSON.stringify(data))
    formData.append('is_edit', is_edit)

    console.log(formData)
    let headers = new HttpHeaders({
      'mimeType': "multipart/form-data",
      'Authorization': 'Bearer ' + localStorage.getItem('access_token')
    });


   let options = {
       headers: headers,
       withCredentials: true
     };

     
     return this.http.post(ENV.apiServer + '/register-employee',formData, options)
     .catch((error: HttpErrorResponse | any) => {
           return Observable.throw(error);
     });

    }

    Logout(){
      let headers = new HttpHeaders({
        'Accept' : 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access_token')
      });
  
  
     let options = {
         headers: headers,
         withCredentials: true
       };
     
    return this.http.get(ENV.apiServer + '/logout', options)
        .catch((error: HttpErrorResponse | any) => {
        return Observable.throw(error);
      });
     }
}
