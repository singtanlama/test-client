import { Injectable } from '@angular/core';

import {Http, Response}          from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { ENV } from '../env';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ImportService {

  constructor(private _http:Http,private http:HttpClient) { }
    importCSVandExcelFile(file): Observable<any> {
      let formData: FormData = new FormData();
      console.log(file,file.name)
      if (file) {
        formData.append('import_file', file, file.name);
      }
      
    // let headers = new Headers({
    //   'mimeType': "multipart/form-data",
    //   'Authorization': 'Bearer ' + localStorage.getItem('access_token')
    // });
    // let options = new RequestOptions({headers: headers, withCredentials: true});
    // return this._http.post(ENV.apiServer + '/import-file',formData, options)
    //     .map(this.extractData)
    //     .catch(this.handleError);

        let headers = new HttpHeaders({
          'mimeType': "multipart/form-data",
          'Authorization': 'Bearer ' + localStorage.getItem('access_token')
        });
    
    
       let options = {
           headers: headers,
           withCredentials: true
         };
    
    
    
        return this.http.post(ENV.apiServer + '/import-file',formData, options)
            .catch((error: HttpErrorResponse | any) => {
                  return Observable.throw(error);
        });
   }
}

