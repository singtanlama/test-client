import { Injectable } from '@angular/core';
import { ENV } from '../env';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Headers, RequestOptions, Response, Http } from "@angular/http";
import { throwError, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http: Http) { }
  public login(data) {
       let headers = new Headers({ 'Content-Type': 'application/json',
       'Accept' : 'application/json',
     // 'Authorization' : 'Bearer '+ token,
      });
      let options = new RequestOptions({ headers: headers, withCredentials: true });
       return this.http.post(ENV.apiServer + '/login', data, options)
        .map(this.extractData)
        .catch(this.handleError);
      }
      public extractData(res: Response) {
        let response = res.json();
        return response || {};
      }
      public handleError(error: Response | any) {
        return Observable.throw(error);
      }
}
