import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public static receiveNotification: EventEmitter<any> = new EventEmitter();

  constructor() { }
  public pushNotification(notification: string, type: string) {
    NotificationService.receiveNotification.emit({
        notification: notification, type: type
    });
  }
  public getNotification(): Observable<any> {
    return new Observable(observer => {
        NotificationService.receiveNotification.subscribe(res=>{
            observer.next(res)
        })
    })
}
}
